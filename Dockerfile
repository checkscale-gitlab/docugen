FROM ubuntu:latest
MAINTAINER Carl Schwan <schwancarl@protonmail.com>

ENV DEBIAN_FRONTEND=noninteractive

RUN rm /bin/sh && ln -s /bin/bash /bin/sh && \
    sed -i 's/^mesg n$/tty -s \&\& mesg n/g' /root/.profile

RUN apt-get update

RUN apt-get install -y build-essential curl git

RUN curl -sSL https://github.com/jgm/pandoc/releases/download/2.3.1/pandoc-2.3.1-1-amd64.deb -o/tmp/pandoc.deb && \
    dpkg -i /tmp/pandoc.deb && \
    rm -f /tmp/pandoc.deb

RUN apt-get install -y texlive-full latexmk

RUN apt-get install -y fonts-wqy-microhei fonts-wqy-zenhei texlive-fonts-recommended texlive-fonts-extra 

RUN mkdir -p /workspace
VOLUME ["/workspace"]
WORKDIR /workspace

# Define default command.
CMD ["bash"]
